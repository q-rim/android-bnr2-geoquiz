package com.example.kyurim.bnr1_geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {

  public static final String EXTRA_ANSWER_IS_TRUE = "com.example.kyurim.bnr1_geoquiz.answer_is_true";
  public static final String EXTRA_ANSWER_IS_SHOWN = "com.example.kyurim.bnr1_geoquiz.answer_shown";

  private boolean mAnswerIsTrue;
  private TextView mAnswerTextView;
  private Button mShowAnswerButton;

  private void setmAnswerShownResult(boolean isAnswerShown) {
    Intent data = new Intent();
    data.putExtra(EXTRA_ANSWER_IS_SHOWN, isAnswerShown);
    setResult(RESULT_OK, data);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cheat);

    // ----- Receiving Data from QuizActivity -----
    mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);

    mAnswerTextView = (TextView)findViewById(R.id.answerTextView);

    // Answer is only shown when the user presses the button.
    setmAnswerShownResult(false);     // isAnswerShown set to false initially

    mShowAnswerButton = (Button)findViewById(R.id.showAnswerButton);
    mShowAnswerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // set the answer on the mAnswerTextView
        mAnswerTextView.setText("the answer is: " + mAnswerIsTrue);
        Log.d("---mAnswerIsTrue: ", "" + mAnswerIsTrue);

        // ---- Send Data back to QuizActivity -----
        setmAnswerShownResult(true);    // isAnswerShown set to true
      }
    });
  }
}
