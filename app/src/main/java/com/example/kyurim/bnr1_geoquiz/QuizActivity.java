package com.example.kyurim.bnr1_geoquiz;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

  private static final String TAG = "--------QuizActivity:";
  private static final String KEY_INDEX = "index";            // the key-value pair that will be stored in the bundle.

  private Button mTrueButton;
  private Button mFalseButton;
  private Button mCheatButton;
  private Button mhasCheatedButton;
  private ImageButton mNextButton;
  private ImageButton mPreviousButton;
  private TextView mQuestionTextView;
  private TextView mApiTextView;
  private boolean mIsCheater;

  private int mCurrIndex = 0;

  private TrueFalse[] mQuestionBank = new TrueFalse[] {
    new TrueFalse(R.string.question_ocean, true),
    new TrueFalse(R.string.question_middle_east, false),
    new TrueFalse(R.string.question_africa, false),
    new TrueFalse(R.string.question_americas, true),
    new TrueFalse(R.string.question_asia, true),
  };

  // ----- Getting Data back from CheatActivity ------
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (data == null) {
      return;
    }
    // getting back Boolean from CheatActivity to see if there has been cheating
    mIsCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_IS_SHOWN, false);
  }

  @TargetApi(11)    // This statement maybe needed on top of the if Statement in making the code compatible with HoneyComb(API=11)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "onCreate(Bundle) called");
    Toast.makeText(QuizActivity.this, "Program Started: onCreate()", Toast.LENGTH_SHORT).show();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_quiz);

    // Making the code compatible with different Android Versions.
    Log.d("TAG", "---- ####Build.VERSION.SDK_INT: " + Build.VERSION.SDK_INT);
    Log.d("TAG", "---- ####Build.VERSION_CODES.HONEYCOMB: " + Build.VERSION_CODES.HONEYCOMB);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
      // ActionBar Subtitle
      android.support.v7.app.ActionBar actionBar = getSupportActionBar();
      actionBar.setSubtitle(R.string.QuizActivity_ActionBar_Subtitle);
    }

    // Retrieving Data Between Rotation
    if (savedInstanceState != null) {
      Log.i(TAG, "onCreate():  checking for savedInstanceState");
      mCurrIndex = savedInstanceState.getInt(KEY_INDEX, 0);
    }
    updateQuestion();

    mApiTextView = (TextView)findViewById(R.id.apiTextView);
    mApiTextView.setText("Build.VERSION.SDK_INT: " + Build.VERSION.SDK_INT
                    + "\nBuild.VERSION_CODES.HONEYCOMB: " + Build.VERSION_CODES.HONEYCOMB
                    + "\nBuild.VERSION_CODES.ICE_CREAM_SANDWICH: " + Build.VERSION_CODES.ICE_CREAM_SANDWICH
                    + "\nBuild.VERSION_CODES.JELLY_BEAN: " + Build.VERSION_CODES.JELLY_BEAN
                    + "\nBuild.VERSION_CODES.KITKAT: " + Build.VERSION_CODES.KITKAT
                    + "\nBuild.VERSION_CODES.LOLLIPOP: " + Build.VERSION_CODES.LOLLIPOP
                    + "\nBuild.VERSION_CODES.MARSHMALLO: N/A");


    mTrueButton = (Button)findViewById(R.id.true_button);
    mTrueButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkAnswer(true);
      }
    });

    mFalseButton = (Button)findViewById(R.id.false_button);
    mFalseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkAnswer(false);
      }
    });

    mPreviousButton = (ImageButton)findViewById(R.id.previous_button);
    mPreviousButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mCurrIndex == 0) {                  // update with the correct negative cyclical modulo statement.
          mCurrIndex = 4;
        } else {
          mCurrIndex = (mCurrIndex - 1) % mQuestionBank.length;
        }
        updateQuestion();
      }
    });

    mNextButton = (ImageButton)findViewById(R.id.next_button);
    mNextButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mCurrIndex = (mCurrIndex + 1) % mQuestionBank.length;
        updateQuestion();
      }
    });

    mCheatButton = (Button)findViewById(R.id.cheat_button);
    mCheatButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        // ----- Start Cheat Activity -----
        Intent i = new Intent(QuizActivity.this, CheatActivity.class);

        // ----- Pass Data to CheatActivity -----
        boolean answerIsTrue = mQuestionBank[mCurrIndex].isTrueQuestion();
        i.putExtra(CheatActivity.EXTRA_ANSWER_IS_SHOWN, answerIsTrue);

        // ----- Getting Data back from CheatActivity -----
        //startActivity(i);           // used for starting activity only.
        startActivityForResult(i, 0);  // used for starting activity and getting back result from ChildActivity.
        // QuizActivity will always start one type of ChildActivity, hence arg2=0
      }
    });

    mhasCheatedButton = (Button)findViewById(R.id.hasCheated_button);
    mhasCheatedButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // ----- Testing the Data back from CheatActivity -----
        Log.d("TAG", "" + mIsCheater);                                                                      // show cheating status - terminal
        Toast.makeText(QuizActivity.this, "Was there cheating?: "+mIsCheater, Toast.LENGTH_SHORT).show();   // show cheating status - toast message
      }
    });
  }

  private void checkAnswer(boolean userAnswer) {
    boolean answer = mQuestionBank[mCurrIndex].isTrueQuestion();
    if (mIsCheater) {
      Toast.makeText(QuizActivity.this, R.string.judgement_toast, Toast.LENGTH_SHORT).show();   // if cheating occured, show message
    } else {
      if (answer == userAnswer) {
        Toast.makeText(QuizActivity.this, R.string.correct_toast_message, Toast.LENGTH_SHORT).show();
        mCurrIndex = (mCurrIndex + 1) % mQuestionBank.length;         // if answer is correct, move to next question
        updateQuestion();
      } else {
        Toast.makeText(QuizActivity.this, R.string.incorrect_toast_message, Toast.LENGTH_SHORT).show();
      }
    }
  }

  private void updateQuestion() {
    mQuestionTextView = (TextView)findViewById(R.id.question_textview);
    mQuestionTextView.setOnClickListener(new View.OnClickListener() {     //  touching TextView makes it move to next question
      @Override
      public void onClick(View v) {
        Toast.makeText(QuizActivity.this, "updateQuestion():  \nmQuestionTextView pressed", Toast.LENGTH_SHORT).show();
        mCurrIndex = (mCurrIndex + 1) % mQuestionBank.length;
        updateQuestion();
      }
    });

    String question = getString(mQuestionBank[mCurrIndex].getQuestion());
    boolean answer = mQuestionBank[mCurrIndex].isTrueQuestion();
    mQuestionTextView.setText("# " + mCurrIndex + "\nQ:     " + question + "\n" + "A:     " + answer);
  }

  // Saving Data Between Rotation
  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    Log.i(TAG, "onSaveInstanceState()");
    super.onSaveInstanceState(savedInstanceState);
    savedInstanceState.putInt(KEY_INDEX, mCurrIndex);    // save the mCurrIndex data into savedInstanceState
  }
}
